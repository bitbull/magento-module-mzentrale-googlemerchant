<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
require_once realpath(__DIR__ . '/../..') . '/abstract.php';

/**
 * Shopping Feed Generate Script
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Shell_ShoppingFeed extends Mage_Shell_Abstract
{
    /**
     * Run script
     */
    public function run()
    {
        /** @var Mzentrale_GoogleMerchants_Model_Shopping_Feed $feed */
        $feed      = Mage::getModel('mzgooglemerchants/shopping_feed');
        $emulation = Mage::getSingleton('core/app_emulation');
        foreach ($this->getStores($this->getArg('s')) as $store) {
            if ($store->getIsActive() && Mage::helper('mzgooglemerchants/shopping')->isActive($store)) {
                $environment = $emulation->startEnvironmentEmulation($store->getId());
                $feed->getXml()->save($this->getExportDir() . sprintf('google_shopping_%s.xml', $store->getCode()));
                $emulation->stopEnvironmentEmulation($environment);
            }
        }
    }

    /**
     * Get stores for export
     *
     * @param null $store
     *
     * @return array
     */
    public function getStores($store = null)
    {
        if ($store) {
            return array(Mage::app()->getStore($store));
        }
        return Mage::app()->getStores();
    }

    /**
     * Get export dir and make sure it exists
     *
     * @return string
     */
    public function getExportDir()
    {
        $dir = Mage::getBaseDir('media') . '/export/';
        Mage::getConfig()->createDirIfNotExists($dir);
        return $dir;
    }

    protected function _applyPhpVariables()
    {
        parent::_applyPhpVariables();
        set_time_limit(0);
        ini_set('memory_limit', '1024M');
    }
}

$shell = new Mzentrale_GoogleMerchants_Shell_ShoppingFeed();
$shell->run();
