<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Trusted Stores Badge
 *
 * @method Mage_Catalog_Model_Product getProduct()
 * @method setProduct(Mage_Catalog_Model_Product $product)
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Block_TrustedStores_Badge extends Mage_Core_Block_Template
{
    /** @var array Parameters */
    protected $_params = array();

    /**
     * Internal constructor
     *
     * Sets the default template if needed.
     */
    protected function _construct()
    {
        parent::_construct();
        if (!$this->getTemplate()) {
            $this->setTemplate('mzentrale/google_merchants/trusted_stores/badge.phtml');
        }
    }

    /**
     * Get parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * Get parameter by key
     *
     * @param string $key Parameter key
     *
     * @return null|mixed
     */
    public function getParam($key)
    {
        return isset($this->_params[$key]) ? $this->_params[$key] : null;
    }

    /**
     * Set parameter value
     *
     * @param string $key   Parameter key
     * @param mixed  $value Parameter value
     */
    public function setParam($key, $value)
    {
        $this->_params[$key] = $value;
    }

    /**
     * Configure block before rendering
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        /* @var $helper Mzentrale_GoogleMerchants_Helper_TrustedStores */
        $helper = Mage::helper('mzgooglemerchants/trustedStores');
        $this->setParam('id', $helper->getStoreId());
        $this->setParam('locale', $helper->getLocale());
        $this->setParam('badge_position', $helper->getBadgePosition());
        if ($helper->isUserDefined()) {
            $this->setParam('badge_container', $helper->getBadgeContainer());
        }

        /* @var $shoppingHelper Mzentrale_GoogleMerchants_Helper_Shopping */
        $shoppingHelper = Mage::helper('mzgooglemerchants/shopping');
        if ($shoppingHelper->isActive()) {
            if ($this->getProduct()) {
                $this->setParam('google_base_offer_id', $this->getProduct()->getSku());
            }
            $this->setParam('google_base_subaccount_id', $shoppingHelper->getAccountId());
            $this->setParam('google_base_country', $shoppingHelper->getCountry());
            $this->setParam('google_base_language', $shoppingHelper->getLanguage());
        }

        return parent::_beforeToHtml();
    }
}
