<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2015 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Abstract Form Select Field
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2015 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
abstract class Mzentrale_GoogleMerchants_Block_Adminhtml_Form_Field_Abstract extends Mage_Adminhtml_Block_Html_Select
{
    protected function _prepareLayout()
    {
        $this->_addOptions();
        $this->setData('is_render_to_js_template', true);
        $this->setWidth(120);
        return parent::_prepareLayout();
    }

    /**
     * Add select options
     *
     * Can be implemented in subclasses
     */
    protected function _addOptions()
    {
    }

    /**
     * Set field width
     *
     * @param int $width Width
     */
    public function setWidth($width)
    {
        $this->setData('extra_params', sprintf('style="width:%dpx"', $width));
    }

    /**
     * Set input name
     *
     * @param string $value Field name
     *
     * @return Mage_Core_Block_Abstract
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}
