<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2015 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Abstract Array Field
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2015 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
abstract class Mzentrale_GoogleMerchants_Block_Adminhtml_System_Config_Array_Abstract
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    /** @var array Field renderers */
    protected $_fieldRenderers = array();

    /**
     * Get field renderer
     *
     * @param string $field Field name
     *
     * @return Mzentrale_GoogleMerchants_Block_Adminhtml_System_Config_Array_Abstract
     */
    protected function _getRenderer($field)
    {
        if (!$this->hasData('_renderer_' . $field) && isset($this->_fieldRenderers[$field])) {
            $block = $this->getLayout()->createBlock($this->_fieldRenderers[$field]);
            $this->setData('_renderer_' . $field, $block);
        }
        return $this->_getData('_renderer_' . $field);
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        foreach (array_keys($this->_fieldRenderers) as $field) {
            $hash = $this->_getRenderer($field)->calcOptionHash($row->getData($field));
            $row->setData('option_extra_attr_' . $hash, 'selected="selected"');
        }
    }

    /**
     * Get element HTML
     *
     * Surround the element with a <div> in order for the
     * dependence block to work correctly.
     *
     * @param Varien_Data_Form_Element_Abstract $element Form element
     *
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return sprintf('<div id="%s">%s</div>', $element->getHtmlId(), parent::_getElementHtml($element));
    }
}
