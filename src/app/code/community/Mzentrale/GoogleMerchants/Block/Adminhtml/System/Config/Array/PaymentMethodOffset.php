<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2015 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Payment Method Offset Array Field
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2015 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Block_Adminhtml_System_Config_Array_PaymentMethodOffset
    extends Mzentrale_GoogleMerchants_Block_Adminhtml_System_Config_Array_Abstract
{
    protected $_fieldRenderers = array(
        'payment_method' => 'mzgooglemerchants/adminhtml_form_field_paymentMethod',
    );

    protected function _prepareToRender()
    {
        $this->addColumn('payment_method', array(
            'label'    => $this->__('Payment Method'),
            'renderer' => $this->_getRenderer('payment_method'),
        ));

        $this->addColumn('offset', array(
            'label' => $this->__('Offset'),
            'style' => 'width:100px',
            'class' => 'required-entry validate-not-negative-number'
        ));

        $this->_addAfter = false;
        return $this;
    }
}
