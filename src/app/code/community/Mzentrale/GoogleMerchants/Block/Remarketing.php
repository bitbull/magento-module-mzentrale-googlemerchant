<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Remarketing Block
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Block_Remarketing extends Mage_Core_Block_Template
{
    /** @var string Model name */
    protected $_modelName = 'mzgooglemerchants/remarketing_other';

    /** @var Mzentrale_GoogleMerchants_Model_Remarketing_Abstract Model */
    protected $_model;

    /**
     * Internal constructor
     *
     * Sets the default template if needed.
     */
    protected function _construct()
    {
        parent::_construct();
        if (!$this->getTemplate()) {
            $this->setTemplate('mzentrale/google_merchants/remarketing.phtml');
        }
    }

    /**
     * Get param model
     *
     * @return Mzentrale_GoogleMerchants_Model_Remarketing_Abstract
     * @throws Mage_Core_Exception
     */
    public function getModel()
    {
        if (is_null($this->_model)) {
            $model = Mage::getModel($this->getModelName());
            if ($model instanceof Mzentrale_GoogleMerchants_Model_Remarketing_Abstract) {
                $this->_model = $model;
            } else {
                Mage::throwException('Invalid model name: ' . $this->getModelName());
            }
        }
        return $this->_model;
    }

    /**
     * Get remarketing params
     *
     * Params are read from
     *
     * @return array
     */
    public function getParams()
    {
        return $this->getModel()->getParams();
    }

    /**
     * Set remarketing parameter
     *
     * This method can be used to customise
     * block output, e.g. through layout XML.
     *
     * @param string $key   Parameter key
     * @param mixed  $value Parameter value
     */
    public function setParam($key, $value)
    {
        $this->getModel()->setParam($key, $value);
    }

    /**
     * Get model name
     *
     * @return string
     */
    public function getModelName()
    {
        return $this->_modelName;
    }

    /**
     * Set model name
     *
     * @param string $name
     */
    public function setModelName($name)
    {
        $this->_modelName = (string) $name;
        $this->_model     = null;
    }

    /**
     * Get conversion ID
     *
     * @return string
     */
    public function getConversionId()
    {
        return $this->helper('mzgooglemerchants/remarketing')->getConversionId();
    }

    /**
     * Get pixel URL, used in <noscript> implementation
     *
     * @return string
     */
    public function getPixelUrl()
    {
        $params = array();
        foreach ($this->getParams() as $key => $value) {
            $params[] = sprintf('%s=%s', $key, implode(',', (array) $value));
        }

        $url   = '//googleads.g.doubleclick.net/pagead/viewthroughconversion/%s/';
        $query = array(
            'value'  => 0,
            'guid'   => 'ON',
            'script' => 0,
            'data'   => implode(';', $params),
        );
        return sprintf($url, $this->getConversionId()) . '?' . http_build_query($query);
    }
}
