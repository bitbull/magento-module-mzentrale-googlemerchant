<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Remarketing Helper
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Helper_Remarketing extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ACTIVE        = 'mzgooglemerchants/remarketing/active';
    const XML_PATH_CONVERSION_ID = 'mzgooglemerchants/remarketing/conversion_id';

    /**
     * Is remarketing active?
     *
     * @param int $store Store ID
     *
     * @return bool
     */
    public function isActive($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE, $store);
    }

    /**
     * Get conversion ID
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getConversionId($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_CONVERSION_ID, $store);
    }
}
