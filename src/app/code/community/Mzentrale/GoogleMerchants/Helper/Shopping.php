<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Google Shopping Helper
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Helper_Shopping extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ACTIVE         = 'mzgooglemerchants/shopping/active';
    const XML_PATH_ACCOUNT_ID     = 'mzgooglemerchants/shopping/account_id';
    const XML_PATH_COUNTRY        = 'mzgooglemerchants/shopping/country';
    const XML_PATH_LANGUAGE       = 'mzgooglemerchants/shopping/language';
    const XML_PATH_EXPORT_SOLDOUT = 'mzgooglemerchants/shopping/export_soldout';
    const XML_PATH_IMAGE_TYPE     = 'mzgooglemerchants/shopping/image_type';

    /**
     * Is Google Shopping active?
     *
     * @param int $store Store ID
     *
     * @return bool
     */
    public function isActive($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE, $store);
    }

    /**
     * Get Google Shopping Account ID
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getAccountId($store = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_ACCOUNT_ID, $store);
    }

    /**
     * Get Google Shopping country
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getCountry($store = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_COUNTRY, $store);
    }

    /**
     * Get Google Shopping language
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getLanguage($store = null)
    {
        return (string) Mage::getStoreConfig(self::XML_PATH_LANGUAGE, $store);
    }

    /**
     * Export soldout products?
     *
     * @param int $store Store ID
     *
     * @return bool
     */
    public function exportSoldout($store = null)
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_EXPORT_SOLDOUT, $store);
    }

    /**
     * Get image type used in feeds
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getImageType($store = null)
    {
        return Mage::getStoreConfig(self::XML_PATH_IMAGE_TYPE, $store);
    }

    /**
     * Filter string
     *
     * Removes whitespace (tabs, newlines) and converts it to single spaces.
     * Also removes html tags.
     *
     * @param string $text
     *
     * @return string
     */
    public function filterString($text)
    {
        $text = strip_tags($text);
        $text = html_entity_decode($text);
        $text = preg_replace('/\s+/', ' ', $text);
        return $text;
    }

    /**
     * Format price
     *
     * @param float $price Price
     *
     * @return string
     */
    public function formatPrice($price)
    {
        return sprintf('%.2F %s', $price, Mage::app()->getStore()->getDefaultCurrencyCode());
    }

    /**
     * Get Google product category
     *
     * @param array $categoryIds Category IDs
     *
     * @return string
     */
    public function getGoogleProductCategory(array $categoryIds)
    {
        if (count($categoryIds) == 0) {
            return '';
        }

        $collection = Mage::getResourceModel('catalog/category_collection');
        $collection->addIdFilter($categoryIds)
            ->addIsActiveFilter()
            ->addAttributeToSelect('google_product_category', 'inner')
            ->addAttributeToFilter('google_product_category', array('notnull' => true))
            ->addAttributeToFilter('google_product_category', array('neq' => ''))
            ->setOrder('level', Varien_Data_Collection::SORT_ORDER_DESC)
            ->setPageSize(1);

        return $collection->getFirstItem()->getData('google_product_category');
    }
}
