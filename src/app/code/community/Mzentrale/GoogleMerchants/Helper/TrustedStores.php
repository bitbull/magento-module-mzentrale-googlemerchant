<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Trusted Stores Helper
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Helper_TrustedStores extends Mage_Core_Helper_Abstract
{
    const XML_PATH_WEEKEND = 'general/locale/weekend';
    const XML_PATH_CONFIG  = 'mzgooglemerchants/trusted_stores/%s';

    /**
     * Get configuration value
     *
     * @param string $param Parameter name
     * @param int    $store ID
     *
     * @return mixed
     */
    public function getConfig($param, $store = null)
    {
        return Mage::getStoreConfig(sprintf(self::XML_PATH_CONFIG, $param), $store);
    }

    /**
     * Is Trusted Stores active?
     *
     * @param int $store Store ID
     *
     * @return bool
     */
    public function isActive($store = null)
    {
        return (bool) $this->getConfig('active', $store);
    }

    /**
     * Get store ID
     *
     * @param int $store Store ID
     *
     * @return int
     */
    public function getStoreId($store = null)
    {
        return (int) $this->getConfig('store_id', $store);
    }

    /**
     * Get badge position
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getBadgePosition($store = null)
    {
        return (string) $this->getConfig('badge_position', $store);
    }

    /**
     * Is badge position user-defined?
     *
     * @param null $store Store ID
     *
     * @return bool
     */
    public function isUserDefined($store = null)
    {
        $ud = Mzentrale_GoogleMerchants_Model_System_Config_Source_BadgePosition::POSITION_USER_DEFINED;
        return $this->getBadgePosition($store) == $ud;
    }

    /**
     * Get badge container
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getBadgeContainer($store = null)
    {
        return (string) $this->getConfig('badge_container', $store);
    }

    /**
     * Get current locale
     *
     * @return string
     */
    public function getLocale()
    {
        return (string) Mage::app()->getLocale()->getLocale();
    }

    /**
     * Get order domain
     *
     * @param int $store Store ID
     *
     * @return string
     */
    public function getOrderDomain($store = null)
    {
        $store = Mage::app()->getStore($store);
        return parse_url($store->getBaseUrl(), PHP_URL_HOST);
    }

    /**
     * Get weekend
     *
     * @param int $store Store ID
     *
     * @return array
     */
    protected function getWeekend($store = null)
    {
        return explode(',', Mage::getStoreConfig(self::XML_PATH_WEEKEND, $store));
    }

    /**
     * Get shipping offset
     *
     * @param int $store Store ID
     *
     * @return int
     */
    public function getShippingOffset($store = null)
    {
        return (int) $this->getConfig('shipping_offset', $store);
    }

    /**
     * Get delivery offset
     *
     * @param int $store Store ID
     *
     * @return int
     */
    public function getDeliveryOffset($store = null)
    {
        return (int) $this->getConfig('delivery_offset', $store);
    }

    /**
     * Get payment method offset
     *
     * @param string $paymentMethod Payment method
     * @param int    $store         Store ID
     *
     * @return int
     */
    public function getPaymentMethodOffset($paymentMethod, $store = null)
    {
        foreach ((array) $this->getConfig('payment_method_offset', $store) as $offset) {
            if ($offset['payment_method'] == $paymentMethod) {
                return (int) $offset['offset'];
            }
        }
        return 0;
    }

    /**
     * Get estimated shipping date
     *
     * @param Mage_Sales_Model_Order $order  Order
     * @param string                 $format Date format
     *
     * @return string
     */
    public function getEstimatedShippingDate(Mage_Sales_Model_Order $order, $format = 'Y-m-d')
    {
        $storeId = $order->getStoreId();
        $offset  = $this->getShippingOffset($storeId);
        $offset += $this->getPaymentMethodOffset($order->getPayment()->getMethod(), $storeId);
        $date = $this->_addOffset($order->getCreatedAtStoreDate(), $offset, $storeId);
        return $date->toString($format, 'php');
    }

    /**
     * Get estimated shipping date
     *
     * @param Mage_Sales_Model_Order $order  Order
     * @param string                 $format Date format
     *
     * @return string
     */
    public function getEstimatedDeliveryDate(Mage_Sales_Model_Order $order, $format = 'Y-m-d')
    {
        $storeId = $order->getStoreId();
        $offset  = $this->getShippingOffset($storeId);
        $offset += $this->getPaymentMethodOffset($order->getPayment()->getMethod(), $storeId);
        $offset += $this->getDeliveryOffset($storeId);
        $date = $this->_addOffset($order->getCreatedAtStoreDate(), $offset, $storeId);
        return $date->toString($format, 'php');
    }

    /**
     * Add offset to date
     *
     * @param Zend_Date $date  Date object
     * @param int       $days  Offset in days
     * @param int       $store Store ID
     *
     * @return Zend_Date
     */
    protected function _addOffset(Zend_Date $date, $days, $store = null)
    {
        $weekend = $this->getWeekend($store);
        for ($i = 0; $i < $days; $i++) {
            $date->addDay(1);
            while (in_array($date->get(Zend_Date::WEEKDAY_DIGIT), $weekend)) {
                $date->addDay(1);
            }
        }
        return $date;
    }

    /**
     * Does order have virtual products?
     *
     * @param Mage_Sales_Model_Order $order Order
     *
     * @return bool
     */
    public function hasVirtualProducts(Mage_Sales_Model_Order $order)
    {
        $flags = $order->getItemsCollection()->getColumnValues('is_virtual');
        return count(array_filter($flags)) > 0;
    }
}
