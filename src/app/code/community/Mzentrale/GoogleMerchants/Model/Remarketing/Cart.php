<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * 'Cart' Remarketing Model
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Model_Remarketing_Cart extends Mzentrale_GoogleMerchants_Model_Remarketing_Abstract
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        $quote = $this->getQuote();
        if ($quote) {
            $this->setParam('ecomm_pagetype', 'cart');
            $this->setParam('ecomm_prodid', $this->getProductIds($quote));
            $this->setParam('ecomm_quantity', $this->getQuantities($quote));
            $this->setParam('ecomm_totalvalue', $this->formatPrice($this->getTotalValue($quote)));
        }
    }

    /**
     * Get product IDs
     *
     * The ID used in this case is the SKU: when
     * dealing with external services, it is better
     * not to use Magento internal IDs.
     *
     * @param Mage_Sales_Model_Quote $quote Quote
     *
     * @return array
     */
    public function getProductIds($quote)
    {
        $products = array();
        foreach ($quote->getAllVisibleItems() as $item) {
            $products[] = $this->getItemSku($item);
        }
        return $products;
    }

    /**
     * Get product quantities
     *
     * @param Mage_Sales_Model_Quote $quote Quote
     *
     * @return array
     */
    public function getQuantities($quote)
    {
        $quantities = array();
        foreach ($quote->getAllVisibleItems() as $item) {
            $quantities[] = (int) $item->getQty();
        }
        return $quantities;
    }

    /**
     * Get total value
     *
     * @param Mage_Sales_Model_Quote $quote Quote
     *
     * @return float
     */
    public function getTotalValue($quote)
    {
        return $quote->getBaseGrandTotal();
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return Mage::helper('checkout')->getQuote();
    }
}
