<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * 'Purchase' Remarketing Model
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Model_Remarketing_Purchase extends Mzentrale_GoogleMerchants_Model_Remarketing_Abstract
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        $order = $this->getOrder();
        if ($order) {
            $this->setParam('ecomm_pagetype', 'purchase');
            $this->setParam('ecomm_prodid', $this->getProductIds($order));
            $this->setParam('ecomm_quantity', $this->getQuantities($order));
            $this->setParam('ecomm_totalvalue', (float) $this->formatPrice($this->getTotalValue($order)));
        }
    }

    /**
     * Get product IDs
     *
     * The ID used in this case is the SKU: when
     * dealing with external services, it is better
     * not to use Magento internal IDs.
     *
     * @param Mage_Sales_Model_Order $order Order
     *
     * @return array
     */
    public function getProductIds($order)
    {
        $products = array();
        foreach ($order->getAllVisibleItems() as $item) {
            $products[] = $this->getItemSku($item);
        }
        return $products;
    }

    /**
     * Get product quantities
     *
     * @param Mage_Sales_Model_Order $order Order
     *
     * @return array
     */
    public function getQuantities($order)
    {
        $quantities = array();
        foreach ($order->getAllVisibleItems() as $item) {
            $quantities[] = (int) $item->getQtyOrdered();
        }
        return $quantities;
    }

    /**
     * Get total value
     *
     * @param Mage_Sales_Model_Order $order Order
     *
     * @return float
     */
    public function getTotalValue($order)
    {
        return $order->getBaseGrandTotal();
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::helper('mzgooglemerchants')->getOrder();
    }
}
