<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Remarketing Abstract Model
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
abstract class Mzentrale_GoogleMerchants_Model_Remarketing_Abstract
{
    /** @var array Remarketing parameters */
    protected $_params = array();

    /**
     * Get remarketing parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * Set remarketing parameter
     *
     * @param string $key   Parameter key
     * @param mixed  $value Parameter value
     */
    public function setParam($key, $value)
    {
        $this->_params[(string) $key] = $value;
    }

    /**
     * Format price
     *
     * @param float $price Raw price
     *
     * @return string
     */
    public function formatPrice($price)
    {
        return sprintf('%01.2f', $price);
    }

    /**
     * Get current category
     *
     * @return Mage_Catalog_Model_Category
     */
    public function getCategory()
    {
        return Mage::helper('catalog')->getCategory();
    }

    /**
     * Get current product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return Mage::helper('catalog')->getProduct();
    }

    /**
     * Get item SKU
     *
     * @param Varien_Object $item Quote or order item
     *
     * @return string
     */
    protected function getItemSku(Varien_Object $item)
    {
        $product = $item->getProduct();
        return strval($product ? $product->getData('sku') : $item->getSku());
    }
}
