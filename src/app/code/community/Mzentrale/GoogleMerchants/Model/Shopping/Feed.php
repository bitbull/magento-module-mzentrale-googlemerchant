<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Google Shopping Feed Model
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Model_Shopping_Feed
{
    const XML_NAMESPACE = 'http://base.google.com/ns/1.0';

    /** @var int Collection page size */
    protected $_pageSize = 250;

    public function getData()
    {
        $data = array();
        $page = 1;

        $products = $this->getProductCollection();
        $products->setPageSize($this->getPageSize());
        while ($products->getCurPage() == $page) {
            $products->addCategoryIds();
            /* @var $product Mage_Catalog_Model_Product */
            foreach ($products as $product) {
                if ($product->getTypeId() == Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
                    foreach ($product->getTypeInstance(true)->getUsedProducts(null, $product) as $simple) {
                        if ($this->getHelper()->exportSoldout() || $simple->isSaleable()) {
                            $data[] = array('item' => $this->getProductData($simple, $product));
                        }
                    }
                } else {
                    $data[] = array('item' => $this->getProductData($product));
                }
            }
            $products->clear()
                ->setFlag('category_ids_added', false)
                ->setCurPage(++$page);
        }

        return $data;
    }

    /**
     * Get product data
     *
     * @param Mage_Catalog_Model_Product $product Product
     * @param Mage_Catalog_Model_Product $parent  Parent product
     *
     * @return array
     */
    public function getProductData(Mage_Catalog_Model_Product $product, Mage_Catalog_Model_Product $parent = null)
    {
        $helper = $this->getHelper();

        $data = new Varien_Object(array(
            'g:id'           => $product->getSku(),
            'item_group_id'  => $parent ? $parent->getSku() : null,
            'title'          => $helper->filterString($product->getName()),
            'description'    => $helper->filterString($parent ? $parent->getDescription() : $product->getDescription()),
            'link'           => $parent ? $parent->getProductUrl() : $product->getProductUrl(),
            'g:condition'    => 'new',
            'g:availability' => $product->isSaleable() ? 'in stock' : 'out of stock',
            'g:price'        => $helper->formatPrice((float) $product->getPrice()),
            'g:sale_price'   => $this->getSpecialPrice($product),
            'g:image_link'   => $this->getProductImage($parent ? $parent : $product, $helper->getImageType()),
            'g:gtin'         => $product->getEan() ? $product->getEan() : null,
            'g:brand'        => $this->getAttributeValue($product, 'manufacturer'),
            'g:color'        => $this->getAttributeValue($product, 'color'),
        ));

        $data->setData('g:google_product_category', $this->getProductCategory($parent ? $parent : $product));
        $data->setData('g:product_type', $data->getData('g:google_product_category'));

        Mage::dispatchEvent('google_shopping_feed_product_data', array(
            'feed'         => $this,
            'product'      => $product,
            'parent'       => $parent,
            'product_data' => $data,
        ));

        return $data->getData();
    }

    /**
     * Get product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection()
    {
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('visibility', Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds())
            ->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
            ->addAttributeToFilter('sku', array('notnull' => true))
            ->addAttributeToFilter('sku', array('neq' => ''))
            ->addAttributeToFilter('name', array('neq' => ''))
            ->addAttributeToFilter('price', array('gt' => 0))
        ;
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        return $collection;
    }

    /**
     * Get product image
     *
     * @param Mage_Catalog_Model_Product $product   Product object
     * @param string                     $imageType Image type
     *
     * @return string
     */
    public function getProductImage(Mage_Catalog_Model_Product $product, $imageType)
    {
        return (string) Mage::helper('catalog/image')->init($product, $imageType);
    }

    /**
     * Get special price
     *
     * @param Mage_Catalog_Model_Product $product Product
     *
     * @return string|null
     */
    public function getSpecialPrice($product)
    {
        $price        = (float) $product->getPrice();
        $specialPrice = (float) $product->getSpecialPrice();
        if ($specialPrice > 0 && $specialPrice < $price) {
            return $this->getHelper()->formatPrice($specialPrice);
        }
        return null;
    }

    /**
     * Get Google Shopping product category
     *
     * @param Mage_Catalog_Model_Product $product Product
     *
     * @return string
     */
    public function getProductCategory($product)
    {
        return $this->getHelper()->getGoogleProductCategory($product->getCategoryIds());
    }

    /**
     * Get brand
     *
     * @param Mage_Catalog_Model_Product $product       Product
     * @param string                     $attributeCode Attribute code
     *
     * @return string|null
     */
    public function getAttributeValue($product, $attributeCode)
    {
        $attribute = $product->getResource()->getAttribute($attributeCode);
        if ($attribute && $attribute->usesSource()) {
            return $product->getAttributeText($attributeCode);
        }
        return $product->getDataUsingMethod($attributeCode);
    }

    /**
     * Get product collection page size
     *
     * @return int
     */
    protected function getPageSize()
    {
        return $this->_pageSize;
    }

    /**
     * Get Google Shopping helper
     *
     * @return Mzentrale_GoogleMerchants_Helper_Shopping
     */
    public function getHelper()
    {
        return Mage::helper('mzgooglemerchants/shopping');
    }

    /**
     * Get XML object
     *
     * @return DOMDocument
     */
    public function getXml()
    {
        $xml = new Mage_Xml_Generator();

        $dom               = $xml->getDom();
        $dom->encoding     = 'UTF-8';
        $dom->formatOutput = false;

        $xml->arrayToXml(array(
            'rss' => array(
                '_value'     => array('channel' => $this->getData()),
                '_attribute' => array(
                    'version' => '2.0',
                    'xmlns:g' => self::XML_NAMESPACE,
                    'magento' => Mage::getEdition() . ' ' . Mage::getVersion(),
                ),
            ),
        ));

        return $dom;
    }
}
