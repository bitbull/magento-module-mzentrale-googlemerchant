<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Trusted Stores Badge Position Config Source Model
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class  Mzentrale_GoogleMerchants_Model_System_Config_Source_BadgePosition
{
    const POSITION_BOTTOM_RIGHT = 'BOTTOM_RIGHT';
    const POSITION_BOTTOM_LEFT  = 'BOTTOM_LEFT';
    const POSITION_USER_DEFINED = 'USER_DEFINED';

    /**
     * Get available badge positions
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'label' => Mage::helper('mzgooglemerchants')->__('Bottom right'),
                'value' => self::POSITION_BOTTOM_RIGHT,
            ),
            array(
                'label' => Mage::helper('mzgooglemerchants')->__('Bottom left'),
                'value' => self::POSITION_BOTTOM_LEFT,
            ),
            array(
                'label' => Mage::helper('mzgooglemerchants')->__('User-Defined'),
                'value' => self::POSITION_USER_DEFINED,
            ),
        );
    }
}
