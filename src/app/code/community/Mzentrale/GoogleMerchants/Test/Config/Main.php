<?php
/**
 * NOTICE OF LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * PHP Version 5
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */

/**
 * Module Configuration Test Case
 *
 * @category  Mzentrale
 * @package   Mzentrale_GoogleMerchants
 * @author    Francesco Marangi | mzentrale <f.marangi@mzentrale.de>
 * @copyright 2014 mzentrale GmbH & Co. KG
 * @license   http://opensource.org/licenses/gpl-3.0 GNU General Public License, version 3 (GPLv3)
 * @link      http://www.mzentrale.de/
 */
class Mzentrale_GoogleMerchants_Test_Config_Main extends EcomDev_PHPUnit_Test_Case_Config
{
    public function testModuleIsActive()
    {
        $this->assertModuleIsActive();
    }

    public function testLayoutUpdateDefined()
    {
        $this->assertLayoutFileDefined(
            Mage_Core_Model_App_Area::AREA_FRONTEND,
            'mzentrale/google_merchants.xml'
        );
    }

    /**
     * @dataProvider     dataProvider
     * @dataProviderFile ~/observers
     */
    public function testEventObserversDefined($area, $event, $modelClass, $method)
    {
        $this->assertEventObserverDefined($area, $event, $modelClass, $method);
    }
}
