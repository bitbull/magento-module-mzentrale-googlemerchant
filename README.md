Mzentrale GoogleMerchants Extension
=====================
Google Merchants integration for Magento

Facts
-----
- version: 1.0.0
- extension key: `mzentrale/google-merchants`

Description
-----------
This module adds following features to Magento:

- [Dynamic Remarketing Tag][remarketing] (for information about available parameters see [here][params])
- [Shopping Feed][shopping]
- [Trusted Stores][trusted]

Compatibility
-------------
- Magento >= 1.7

Installation Instructions
-------------------------
1. Install the extension via [Composer][composer] or copy `src` folder contents into your document root.
2. Clear the cache, log out from the admin panel and then log in again.
3. Configure and activate the extension under `System > Configuration > Google Merchants`

Uninstallation
--------------
1. Remove all extension files from your Magento installation

Developer
---------
Francesco Marangi   
[http://www.mzentrale.de/][mzentrale]   
[@fmarangi](https://twitter.com/fmarangi)

License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

Copyright
---------
(c) 2014 [mzentrale | eCommerce - eBusiness][mzentrale]

[mzentrale]:   http://www.mzentrale.de/
[composer]:    http://getcomposer.org/
[remarketing]: https://support.google.com/adwords/answer/3103357
[params]:      https://developers.google.com/adwords-remarketing-tag/parameters
[shopping]:    https://support.google.com/merchants/answer/188494
[trusted]:     https://support.google.com/trustedstoresmerchant/topic/6063044
